Oscilloskopslabben hemifrån

Utöver att läsa igenom laborationshandledningen och besvara förberedelseuppgifterna ska följande utföras för att bli godkänd på oscilloskoplabben.

Ge dig själv en djupare introduktion genom att titta på detta videoklipp 'How to use an oscilloscope': https://www.youtube.com/watch?v=u4zyptPLlJI

Med bas i laborationshandledningen, videoklippet samt annan kompletterande litteratur, besvara följande frågor:

1. Beakta figur 3 i handledningen:    

    a) Vilket samband råder mellan insignalens period T och t1+t2 enligt figuren?
    b) I figuren är triggkretsen inställd på att trigga på en stigande spänningsflank. Vad händer med signalen på skärmen då du istället byter den till att trigga på en fallande spänningsflank?


2. Vad händer med signalen på oscilloskopet om trigger-nivån är inställd så att signalen aldrig korsar dess spänningsnivå?


3. Vilken inställning använder man föredragsvis för att mäta en signal som inte är periodisk med oscilloskopet?


4. Vilken samplingsfrekvens bör ditt digitala oscilloskop minst ha för att mäta en sinus-signal med frekvensen 20 MHz?


5. Om samplingsfrekvensen är för låg uppstår problem för ett digitalt oscilloskop, vad som kallas aliasing. Beskriv detta utifrån en figur.


6. Du mäter följande signal på oscilloskopet: u = [10 + sin(ωt)] Volt. Vad händer när du byter mellan AC och DC koppling?


7. Du vill mäta effektivvärdet för en signal med 'Measure' inställningen på oscilloskopet. Oscilloskopet kan mäta 'peak-to-peak' spänning och en 'RMS' spänning. Vad innebär dessa och vilken bör du välja?


8. Ge exempel på lämpliga tillämpningar av ett oscilloskop när till exempel inte en multimeter är särskilt användbar.

Uppgifterna ska utföras individuellt och skickas in som inlämningsuppgift på live@lund. Deadline är kl 17:00 på fredagen då ni skulle ha haft er laboration, men det är öppet att skicka in från och med nu!

Kontakta mig (anton.samark-roth@nuclear.lu.se) vid frågor.
