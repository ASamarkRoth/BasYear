#Laboration - Plan Pendel#

Läroboken finns inscannad i denna mapp. 

Se labbok för instruktioner. 

##Lägga in i grupplistor##
Första labben, anmäl er i grupper motsvarande hur ni har fördelat er, på liveatlund.

##Syfte##
Syftet med laborationen är inte att utveckla en perfekt kunskap i mekanik, dvs tillräcklig för att härleda relationer. 
Istället är det meningen att lära sig hur en experimentalist arbetar.
Utveckla en metod för att systematiskt undersöka beroenden.
Därefter komma fram med en modell som representerar fenomenet.
Slutligen, ska resultatet av experimentet presenteras i form av en rapport.
Detta lika viktigt som att lära sig att utföra experimentet.

##Execution##

Pos A, l = 29 cm: m = 50 g, theta = 20 -> t = 3.29, 3.24, 3.15 = 1.07 s

Pos A, l = 29 cm: m = 100 g, theta = 20 -> t = 3.15, 3.35, 3.35 = 1.10 s

Pos A, l = 29 cm: m = 50 g, theta = 10 -> t = 2.96, 3.11, 3.23 = 1.10 s

Pos B, l = 40 cm: m = 50 g, theta = 20 -> t = 3.88, 3.75, 3.69 = 1.25 s

Pos C, l = 83 cm: m = 50 g, theta = 20 -> t = 5.38, 5.65, 5.40 = 1.8 s

Pos D, l = 131 cm: m = 50 g, theta = 20 -> t = 6.51, 6.82, 6.82 = 2.2 s

Ansats: $t = kx$. 

Två punkter (A & B): 1.1 = k*29+m, 1.25 = k*40+m ->

k = (1.25-1.1)/(40-29) = 0.014 & m = 0.69.

$t = 1$ -> x = (1 - 0.69)/0.014 = 22 cm

##Redovisning av resultat##

* Noggrannt utförande, gärna med bild av uppställningen.
* Logiska resonemang kring osäkerheter. Vilka resultat kan vi inte skilja från varandra.
* Tabeller, diagram, konstruera modell. Om ni hittar något beroende kan detta modelleras i diagram.
* Jämför med litteraturvärden. Dvs kursboken.

##Konceptuella förklaringar##

* Massan: Högre massa accelereras mer i nedåtrörelse men samtidigt så bromsas den mer i uppåtrörelsen.
* Vinkeln: Större vinkel möjliggör längre acceleration i nedåtrörelse och därmed större hastighet. Även om pendeln rör sig en längre sträcka så färdas den med en större medehastighet.
* Längden: Ju större längd på pendeln desto större cirkelsektor som pendeln måste röra sig. 

##Härledning rörelseekvation och periodtid##
At wikipedia derivations are presented on the basis of force equations, torque equations and energy balance: <https://en.wikipedia.org/wiki/Pendulum_(mathematics)>.

The course book uses the torque equation: $\tau = - mgl\sin (\theta)$ and compares it to Hooke's law: $F=kx$, which if set in motion assumes an harmonic motion with $f = (1/2\pi) (k/m)^{1/2}$. 

Substitution of $k$ and $m = m I^2$, where $I$ is the moment of inertia (for a rotational rigid body (stelkropp)). 

For the simple pendulum: $I = mr^2$, where r is the distance from the pivot point = l.

Hence: $\omega = 2 \pi f = (g/l)^{1/2}$.

Recall: $f = 1/T$.

-> $T = 2\pi * (l/g)^{1/2}$

T = 1 s. -> $l = T^2g / 4\pi^2 = 0.248 m$

## Hur skriva rapport?

Gå igenom de olika delarna av en rapport och vad de ska innehålla.
	
* Inledning/Syfte

Här introduceras läsaren till ämnet och syftet presenteras. Koppla fysiken till verkligheten, i detta fall tex ett pendelur.

* Teori

I teorin ska ni presentera de begrepp och fysik som krävs för att läsaren ska förstå resten av er rapport.
Såsom:

	* Vad är periodisk rörelse och periodtid?
	* Vad är plan pendel?
	* Vad för fysik är detta? (Mekanik)
	* Vad säger litteraturen (dvs forskning) om det fenomen som undersöks?

* Utförande

	* Här beskrivs experimentet så pass noggrannt att läsaren kan reproducera det som gjorts. Exempelvis beskrivs hur längden på pendeln erhölls samt hur tidtagningen gick till.
	* Bild av experimentuppställningen bör även inkluderas.
	* Mätningar, beräkningar och analys sparas till resultat.

* Resultat

	* Introducera och beskriv det presenterade resultatet grundligt och tydligt.
	* Att endast presentera datan i tabeller är bristfälligt. Väldigt svårt för läsaren att snabbt få en överblick av resultatet och era slutsatser.
	* Mätningar presenteras i tabell (med enheter) och även i grafer.

* Diskussion med slutsats

	* Resonemang kring hur osäkerheter påverkar resultatet.
	* Resonemang kring (med fördel i grund på grafer) att längden är den enda parametern som ändrar på periodtiden markant, dvs utöver osäkerheter.
	* Resonemang kring konstruktion av modell baserad på data. Kanske inte beroendet är linjärt.
	* Jämför värde för er modell av längden för periodtid=1 s med annan "forskning" = kursboken.
	* Koppla gärna till er hypotes. Stämde den?


* Referenslista

Gör en ordentlig referenslista till vilken ni referar till. Se exempelvis: http://hv.se.libguides.com/c.php?g=243055&p=1617026

* Rita upp tabell och figur exempel:

Tabelltext:

* Alla tabeller ska ha en tabelltext (över tabellen) som beskriver vad som presenteras i tabellen. Exempel: "Tabell 1. Här presenteras de olika uppmätta periodtiderna för tre olika vinklar."
* Bör ha tabellnummer som refereras till i text.
* All data ska presenteras med textbeskrivning och enhet (ifall möjligt).
* Värden presenteras med lika många värdesiffror, konsekvent.


Figurtext:

* Alla figurer ska ha en figurtext (under figuren) som beskriver vad som visas i figuren. Exempel: "Figur 1. Periodtiden presenteras som funktion av utfallsvinkel."
* Figur ska vara centrerad och stor.
* Ska ha figurnummer som refereras till i text.
* Pilar och text indikerar viktig information i figuren för läsaren.

Plot:

* Plot görs med fördel på datorn, tex i excel.
* x och y axel har tydlig beskrivning med enhet (centrerat under axeln).
* Alla labels är stora och tydliga.
* Markera mätvärden med punkter och eventuella modeller som linjer.
* Inkludera en s.k. legend. Detta är en ruta där ni beskriver vad de olika symbolerna (tex punkter och linjer) representerar i diagrammet.
* Upplösning bra.

* Se filen `kommentarer.txt`

