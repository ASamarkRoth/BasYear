# Vad är Effektivvärde 
Effektivvärde betecknas på engelska root mean square, RMS, och definieras som kvadratroten ur tidsmedelvärdet av storhetens kvadrerade värde. Effektivvärde används för att förenkla beräkning av effekt i växelströmskretsar.
Av definitionen följer att lika stor värmeenergi utvecklas i ett elektriskt värmeelement som ansluts till vägguttaget som till en likspänning på 230 V.

integral(0-T) = A²cos²wt = A² * T / 2

För DC: $P = U^2 /R$

Motstånden tar ut varandra och:

Medel: A²/2 = V_DC^2 -> A = sqrt(2)*V_DC

# Förberedelseuppgifter

### 1
$u_{lampa} = 60 - 0.283 = 59.717$ 

$R_{lampa}/(R_{lampa}+R) * 60 = u_{lampa} -> R_{lampa} = 179.36 $

$P_{medel} = A² / (2*R_{lampa}) \sim 10 V$

### 2 
**a)**: 2.5 T = 9 rutor -> T = 3.6*250 µs -> fq = 1/T = 1111 Hz

**b)**: 4 rutor$ \cdot 0.5 = 2$

**c)**: effektivvärde = 2/sqrt(2) = sqrt(2)


