# Likströmskretsar

## Förberedelseuppgifter

* Något som har missförståtts? -> Gå igenom!

## Syfte

* Lära sig utföra beräkningar och mätningar i likströmskretsar. 
* Användning av amperemeter, voltmeter och ohmmeter -> dvs multimeter.
* I parallellkopplingar, seriekopplingar och mix.


## Upplägg

Laborationsrapport i par: Essentiellt att anteckna!

* Seriekoppling av resistanser.
* Parallell -||-.
* Ström och spänningskarakteristik (cf. förberedelse 2).
* Spänningsmätning i en krets som störs av mätningen.

## Utrustning (gå igenom)
* Resistanser/motstånd.
* Likspänningskälla.
* 2 multimetrar:
	* Com - common = jord som motsvarar minuspol.
	* Spänning: Voltmetern kopplas parallellt över det som ska mätas (skriv detta).
	* Ström: Amperemetern kopplas i serie med det som ska mätas (skriv detta).
	* Exemplifiera med enkelt scenario!
	* Resistans: Ska ske utan spänning. Endast med resistans inkopplad.
	* OBS: Om multimetern är inställd på att mäta ström när du försöker mäta spänning blir det kortslutning i kretsen och multimeterns säkring går (går sönder :))
	* Om du vill mäta ström kopplar du in ett väldigt lågt motstånd (vill ju inte modifiera kretsen) -> parallellt kortslutning.
	* Om du vill mäta spänning kopplar du in ett väldigt stort motstånd parallellt, dvs ingen ström går där och ingen modifikation.

* Kopplingsbräda:

	* Visa hur resistanserna från början bör vara inkopplade, med klart exempel.
	* Diskutera hur resistanserna i en parallellkopplad krets bör mätas.

* Sladdar:

	* Använd gärna olika färger för egen tydlighet.

* Klurigheter:

	* Mäta ström i en parallelkopplad krets.
	* Rita på tavlan för det fall som kommer, dvs tre parallellkopplade.
	* Föreslå: koppla om mellan varje mätning så att syftad resistans är "längst bort" i kopplingen. 

* Rapport: 
	
	* Göra bra tabell: Beräknat och uppmätt i två intilliggande kolumner. 
	* För att rita kretsar: <https://www.digikey.com/schemeit/project/>

## Tips

* Mellan de olika uppgifterna koppla ur alla sladdar och börja om på nytt.
* Koppla en sladd i taget från likspänningskällan och jämför vilken nod detta motsvarar i kopplingsschemat.
* Koppla in kretsen först och sen tänk på multimetern.

* För beräkningar:

	* Relatera till Kirchoffs strömlag och spänningslag för ström i seriekopplad krets och spänning i parallellkopplad krets. 
	* <https://en.wikipedia.org/wiki/Kirchhoff%27s_circuit_laws>

## Övrigt

* Kan finnas anledning till att gå igenom vad som ska studeras i den sista uppgiften!








EXTRA - Hur fungerar multimetern?:
Digital multimeter DMM
They use an analog to digital converter to convert voltage to a digitally displayed number.

There is a scaling circuit to accomodate different ranges.

For current readings they have a small shunt resistor and the voltage A/D section is used to measure the developed voltage across the shunt which is proportional to current.

For resistance they have a current source inside the muiltimeter. The current is applied to the resistor under test and the developed voltage is proportional tot he resistance.

For AC voltage and current, they have an additional circuit that measures the RMS or Average content of the waveform. The average is usually adjusted to correspond to the same value for sine wave inputs as you would get with RMS.
