# Laboration kraftekvation basåret

Printa ut ett par tabeller så att om studenter inte vill skriva på datorn, kan de använda sig av dessa.

## Förberedelseuppgifter

1. Hur ser grafen för ett proportionellt samband ut (t.ex. y är proportionellt mot x)?
	
	* Linjär!?

2. Hur stor tyngd har massan 20 g?
	
	* Tyngd är den kraft som verkar på kroppar i ett gravitationsfält. $G = mg = 0.196$ N.

3. I figuren på nästa sida bryts ljuset i fotocellen, av en plastplatta med bredden 2,0 cm (= ∆ x), under 65 ms. Beräkna vagnens medelfart vid passage av denna fotocell.
	
	* Medelfart = 2 cm / 65 ms = 0.31 m/s.

4. Rita frikroppsdiagram för vagnen med tyngd, enligt figuren på nästa sida.
5. Rita frikroppsdiagram för tyngden, som drar i vagnen, enligt figuren på nästa sida.

Se exempel förberedelse .pdf för frikroppsdiagrammen.

Typiskt misstag: 

* Frikroppsdiagrammen kan ibland vara förvirrande eller sakna viktiga delar.
* Gå igenom syftet med frikroppsdiagram med experimentuppställningen som exempel.

## Bra att tänka på: 

* Placera första fotosensor runt 35 cm. På så sätt kan vagnen accelereras hyfsat till en början och det finns tillräckligt med avstånd så att inte tyngden slår i marken för tidigt.
* Rör inte vagnen på banan om lufttrycket är avstängt.
* Inställning på mätinstrumentet ska vara acceleration. Redo för mätning då ljus lyser på för bägge inputs. Tryck på memory för att gå igenom de tre tidsmätningarna som görs.

Det finns ett spreadsheet in cd där föregående års mätningar gjorts.

## Utförande 

* Börja med att gå igenom syfte med frikroppsdiagram och rita dessa för de två komponenterna i experimentet, vagn med vikt och dragvikten. Ta upp exempel med spänningskraft för dragvikten när det finns friktion på banan! 
* Presentera syfte med denna labb: lära sig samband mellan kraft och acceleration och rörelseekvationer. Egentligen, Newtons första och andra lag. 
* Gå igenom experimentuppställningen noggrannt. Typiskt misstag är att räkna på tiden mellan sensorerna och ta avståndet i första uppgiften!
* Låt härefter studenterna utföra experimentet, dvs låt de välja vikter och starta och stoppa mätningarna. 
* Vid utförda uppgifter ska dessa presenteras för handledaren.


### Det finns tre olika försök: 

1. Dragtyngd och vagnens massa är konstanta och avståndet varieras.

	* Handledare antecknar vikter och resultatet på tavlan.
	* Studenterna ska sedan beräkna accelerationen själva och resonera kring resultatet. 
	* Kan vara kul med ett långt avstånd där vikten slår i marken innan vagnen nått den andra fotosensorn.

2. Dragtyngden ändras medan vagnens massa och dragtyngdens massa hålls konstant.
 
	* Handledare antecknar m2 och m3 och beräknar accelerationen till studenterna och skriver resultatet på tavlan.
	* Studenter försöker bestämma sambandet mellan F3 och a. 
	
3. Dragtyngdens massa hålls konstant medan vagnens massa varieras.

	* Handledare antecknar m2 och beräknar accelerationen till studenterna och skriver resultatet på tavlan.
	* Studenter försöker bestämma sambandet mellan m2 och a. 

Försök hålla studenterna på tråden, avslöja inte slutresultatet, dvs rörelseekvationen förrän absolut sist. Låt studenterna klura och i slutändan försöka få fram ekvationen själva.
	
