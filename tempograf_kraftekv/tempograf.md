# Laboration tempograf basåret


## FÖrberedelseuppgifter

1. Vad menas med medelfart och hur beräknas den.
	
	* Medelfart=medelhastighet: $\delta s / \delta t$.

2. Vad menas med (medel-)acceleration och hur beräknas den.
	
	* $a = \delta v / \delta t$

3. Hur ser den graf ut som har läge på y-axeln och tid på x-axeln, då hastigheten är konstant (större än 0 m/s)? Rita ett exempel.

	* Linjärt ökande.

4. Hur ser den graf ut som har hastighet på y-axeln och tid på x-axeln, då accelerat- ionen är konstant (större än 0 m/s 2 )? Rita ett exempel.

	* Linjärt ökande.

5. Hur lång tid har det gått mellan punkt nr 1 och punkt nr 10 på tempografremsan? Studera figuren av tempografremsan i denna handledning.

	* 10-1 = 9 hundradelars sekund.

Typiskt misstag: 

* Uppg. 5, 0.1 s!
* Vissa ritar accelerationsdiagrammet som kvadratiskt ökande funktion.


## Utförande - Tips utöver handledningen

* 7 V växelspänning från de gula aggregaten ska ges till tempografen. Detta är vibrerande metall med frekvens 50 Hz som slår vid varje gång mittpunkten korsas. (Hur fungerar denna egentligen?)
* Minst två personer behövs för mätning med vagnen. En ska stå och ta emot vagnen så att denna inte ramlar ner på golvet. Om det sker kan de friktionsfria hjulen förstöras och vi har inte många i reserv.
* Det finns endast 4 stationer. Men mätningarna går snabbt så att de kan utföras utan bekymmer för alla ändå.
* Det finns tejp att tillgå för att fästa remsan i det rörliga föremålet.

* Remsornas sidor skiljer sig lite åt. Testa vilken som fungerar bäst. 
* Om tempografen inte ger tydliga markeringar kan dessa bytas ut. 

* Riv av en remsa innan mätning annars kan dess vikt påverka mätningarna.

* Jag tycker det bör tilläggas till rapporten en jämförelse av erhållen acceleration och teoretisk. För detta bör ni mäta avstånd!
