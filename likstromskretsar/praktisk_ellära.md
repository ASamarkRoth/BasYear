# Praktisk ellära laboration basåret

Frågetecken:
	Närvaro - Fått excel sheet!
	Hur fungerar lampa? Resistans + uppvärmning
	

Pre-introduktion:
	Förbered hjälp-lista
	Skriv upp Ohms lag och effektlagen
	Gå igenom hela labben själv
	Fördela utrustningen över bänkarna


## Introduktion:

Syfte:

* Bekanta sig med fysiken bakom elektriska apparater/utrustning som används i vardagen. 
* Koppla enkla elektriska kretsar.

Frihet:

* Som framgår av handledningen så är instruktionerna inte jätte konkreta och detaljerade, dessutom ska det inte skrivas rapport.
* Detta innebär att det finns en stor frihet för er, det är ni som ska driva och själva lära er. 
* Passa på att fundera över hur det verkligen fungerar och vänd er till mig om ni inte lyckas klura ut det så kan vi tillsammans lösa det.

Vad är elektricitet? Gå igenom Ohms lag och effektlagen. Hur kan man se på Ohms lag. Ni har gått mekanik. Spänning = elektrisk potentiell energi och ström är flödet av 

## Fakta

Glödlampa: 

* Glödtråden är gjord av volfram som har hög smältpunkt. I kontakt med syre börja den brinna lätt vid uppvärmning. Därav icke reaktiv ädelgas i glaskolven. 
* Värms upp till 2500 grader, det är vid denna temperatur som mycket av energin blir till ljus. 
* Ljuset skapas av kollisioner mellan elektronerna och atomerna som blir tillräckligt betydelsefulla om glödtråden är tunn, annars kan strömen spridas över hela tråden och detta är inte lika effektivt. 

Elmotorn: <https://electronics.howstuffworks.com/motor4.htm>


## Upplägg:

* Handledningen beskriver olika komponenter som ni har tillgång till och även initiala kopplingsfigurer.
* Följ dessa beskrivningar. 
* Koppla ihop komponenterna utan spänningskälla. Fråga mig innan ni kopplar in denna. Om det blir väntetid så försök förstå vad som ska hända och de olika komponenterna och tänk ut mer som ni vill testa. 
* Genomgång av de olika komponenterna:

	* Transformator
	* Säkringsskåp (kan ni slå antalet säkringar som gick?)
	* Värmeelement
	* Elmotor
	* "Brandlarm" - värmekänslig strömbrytare
	* Summer
	* Lampa
	* Tryckströmbrytare och switch strömbrytare
	* Kopplingslist - förena två ledningar
	* Banankontakter
	* Sladdar
	* Klämmor
	* Stearinljus
	* Skruvmejsel
	

## Delar

* Koppla in säkringsskåpet + transformatorn
* Hur fungerar strömbrytaren?
* Värmeelementet

	* Beskriv vad som händer.
	* Har glödlampan smalare/tjockare tråd? Varför?

* Elmotorn

	* Kan du få den att spinna åt båda hållen

* Brandlarm
	* Följ handledningen.
	* Summer.

* Lampor

	* Koppla i serie och parallel.
	* Vilka skillnader ser ni (ljusstyrka)? Varför?
	* "Bevisa julgransproblemet"

* Extras

	* Koppla in värmeelementet i serie med en lampa. Vad händer? Varför?
	* Samma fast motorn istället för värmeelementet.
	* En strömbrytare för två lampor.
	* Kyl stearinljuset/metallen med fläkten när larmet ljuder.
	* Två switchar för att ändra polaritet och styra fläktens rotation!!! (imponerande)
		
		
**OBS**:
	Glöm ej koppla ur spänningskälla när ni byter koppling
 
