# Optik

För bra genomgång: 

* <http://kiko.fysik.su.se/Kurser/optics/GeoVT14%20_final.pdf>
* <https://www.youtube.com/watch?v=drRL3lJKzA4>

## Förberedelseuppgifter

* Positiv = konvex = samlingslins
* Negativ = konkav = spridningslins
1. Beskriving: En positiv lins har brännvidden 3,5 cm. Ett 4,0 cm högt föremål placeras 7,0 cm framför linsen. Rita en skalenlig strålkonstruktion över avbildningen. Välj skala 1:1 både i x-led och y-led. 

* Rita denna som repetition. 
* Jämför med linsformeln: uppochnervänd, di = d0.

2. Linsformeln: 1/d0 + 1/di = 1/f

	* d0 = avståndet mellan föremålet som ska avbildas och linsens mitt
	* di = avståndet mellan det avbildade föremålet och linsens mitt (negativt värde om avbildning på samma sida som föremål)
	* f = linsens brännvidd eller fokallängd, för negativ lins -> f < 0

* Förstoring = h_bild/h_objekt = -di/d0

	* Positiv -> rättvänd
	* Negativ -> uppochnervänd
3. 

* reell bild = di > 0, representation av verkligt föremål som bildas av ljusstrålar och kan avbildas på skärm. 
* virtuell bild = di < 0, representation av föremålet bakom linsen. Strålarna divergerar och vi uppfattar att bilden uppstår här. Denna bild kan inte avbildas på en skärm. 

## Syfte
Praktisk erfarenhet av optik med positiv lins med avbildning och verifiera Gauss linsformeln.

## Utrustning

* Optisk bänk, grundsats
* litet bord
* positiv lins
* skärm
* värmeljus
* tändstickor
* linjal

# Uppgifter

Följ labbinstruktionerna!

1. Kontrollera brännvidd

	* Kan använda mobillampa

2. Justera höjd på bord så att bild av stearinljus hamnar mitt på skärm
3. Fånga upp skarp bild av lågan för flertalet avstånd mellan ljus och lins

	* d0	di	
	* 50 	12	uppochnervänd, förminskad (strålgång)
	* 40	13	uppochnervänd, förminskad (strålgång)
	* 30	15	uppochnervänd, förminskad (strålgång)
	* 20	22	uppochnervänd, samma storlek. 
	* 15	35	uppochnervänd, förstorad!
	* 8	-	ingen bild, virtuell rättvänd förstorad bild (titta genom linsen, men var är denna?)

4. Analysera strålgången för ett försök

# Inlämning

* Fyll i tabellen. 
* Kommentera resultatet och jämför med beräkningarna.
* Rita strålgång för ett av försöken.

## Extras:

* Förstorad rättvänd bild med de två postiva linserna.
* Imitera en projektor med diabilden, vad visar den? 
* (Lek med konkav lins)

# Felkällor:

* NEJ! Inga planvågor, ej väsentligt här.
- Optiska element som placeras i strålgången skall alla vara på samma 
höjd och vinkelräta mot den optiska axeln. Se även till att den skärm 
där bilden skall hamna på är vinkelrät mot det infallande ljuset.
- Små avstånd är svårare att mäta till samma relativa precision än 
stora avstånd. Tänk på detta, t.ex. i uppgift 7.1, där du ska mäta 
avståndet mellan två ljusprickar.
- Är linsen tillräckligt tunn?

# Rapport (gammal)

* Förstoring.
* Diskutera användning av positiv lins.
* Digital bild strålgången: <https://ricktu288.github.io/ray-optics/simulator/>


