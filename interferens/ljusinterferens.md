# Ljusinterferens #

Härledning av gitterformeln (nyckeln är L >>> d -> parallella strålar från öppningarna)
[http://hyperphysics.phy-astr.gsu.edu/hbase/phyopt/slits.html#c1](http://hyperphysics.phy-astr.gsu.edu/hbase/phyopt/slits.html#c1)

Mer intressant läsning: <https://www.scribd.com/doc/16117537/Interference-of-Light-It-s-Applications-Interferometry>

## Förberedelseuppgifter ## 
1. Vad är interferens? Superposition av flera vågkällor. 
2. Introducera gitterekvationen: `d sin(theta) = m lambda`
	
	* Många skriver: "θ = Är vinkeln mellan normalaxeln och vågrörelsen där ljuset infaller vid en spalt", vad menas med det?
3. Vilken färg av synligt ljus har längst våglängd? Rött.
4. Gör beräkningar med gitterekvationen: a) theta = 26 ^o, b) theta = 16 ^o.

## Intro ##
Tillämpning av interferens med ljus. Två mål:

* Bestämma det synliga våglängdsområdet.
* Bestämma spåravståndet i en CD-skiva. 

Riktigt go labb. Ni kan se del 1 som ett forskningsexperiment (början av 1800-talet för att kolla om Thomas Young har rätt) och del 2 som en verifikation av industriell produktion av CD-skivor som bör vara 2µm. 

Finns anledning till att härleda gitterekvationen. Koppling till interferens hos studenterna är eventuellt inte där.

## _Del 1_ ##

* Lampa med monterad lins
* Transformator till lampa
* Spalt
* Gitter (600 spalter/mm) i hållare
* A3 papper och linjal

Luta lampan och gittret och projicera interferensmönstret på ett papper (inte nödvändigt att luta lampan).

Men vad menas med att "Placera lampan på en förhöjning en bit in på pappret, så att ljusstrålarnas förlängning bakåt
skär varandra på pappret."?

Notera att: Ljuset bryts!!!

# Resultat # 

* `d = 1e-3/600 = 1.7 µm`
* `_theta_(medel) blå = arctan(8.5/30.5) = 0.27 rad -> lambda = d sin(theta) = 450 nm`
* `_theta_(medel) red = arctan(4.6/8.5) = 0.42 rad -> lambda = 693 nm`

Tips: Använd så långt avstånd som möjligt för att maximera noggrannhet.

## _Del 2_ ## 
* Laserpekare, försök undvik höjd med laserljuset. 
* CD-skiva
* 2 stativ 
* Stativklämmor 
* Linjal

# Resultat #
_Mätt_:

- `l(m2) = 17.7 cm`
- `l(m1) = 51.5 cm`
- `h(laser) = 23 cm`

_Beräkningar_:

* `lambda = 654 nm` (enligt instruktioner)
- `theta(m1) = arctan(h/l(m1)) = 0.42 rad`
- `theta(m2) = arctan(h/l(m2)) = 0.91 rad`
* `d = m*lambda/sin(theta) = 1.60 µm & 1.66 µm`

Hur stort bör spåravståndet vara? 
Enligt följande källa är spåravståndet 1.60 µm.
<http://www.alltomvetenskap.se/nyheter/hur-fungerar-en-cd-skiva>

Tips: Titta på nollte ordningens maximum för att aligna laser och skiva. 

Analys: Alla har erhållit ett lägre spåravstånd för maximum 1. Diskussion ...








