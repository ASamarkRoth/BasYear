# Beräkningar labb likströmskretsar

## 1. Seriekopplad krets 

U0 = 10 V

Total_Ohm = (1 + 1 + 3.3) = 5.3 kOhm

Volt_1kOhm = (1/5.3) = 2 V

Volt_33Ohm = 3.3/5.3 = 6 V

I = 10/5.3 = 0.002 = 2 mA


## 2. Parallellkopplad krets 
a) 500 Ohm

b) (1/R_tot) = 1/R1 + 1/R2 + 1/R3 = 2*0.001+1/3300 = 0.0023
-> R_tot = 435 Ohm

I_1o2 = U/R = 10/1000 = 0.01 A = 10 mA

I3 = 10/3300 = 3 mA

## 4. Störning

U2_mätt = 2.87 V

U2 = R2/(R1+R2+R3) * U0 = 15/30 *10 = 5 V

2.87/5 = 57%

R_p = (R2*Ri)/(R2+Ri)

U2_mätt = Rp/(R1+Rp+R3)*U0 -> Rp = U2(R1+R3)/(U0-U2) = 2.87 * 15/7.13 ~ 6 MOhm

Ri = RpR2/(R2+Rp) = 6 * 15/(15-6) = 90/9 = 10 MOhm

U2vm = Rp/(R1+Rp+R3)*U0 = 6/21 * 10 = 2.85 V
