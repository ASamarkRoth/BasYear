# Förberedelseuppgifter Likströmskretsar

# 1. 

Tre resistanser, R_i varav i=2,3 är parallellkopplade.
R_i = 100, 200, 50 Ohm

R_tot = R1 + R2*R3/(R2+R3) = 140 Ohm

I_1 = 10/140 = 0.071

U_2 = U_3 = 10 - (100/140) * 10 = 2.86 V

I_2 = U_2/R2 = 0.014

I_3 = U_3/R3 = 0.057

# 2

100 och 50 Ohms motstånd.

-> Två linjer med riktningskoefficient motsvarande. 

# 3

U = 230 V, P = 60 W

P = U * I = U^2 / R

R = U^2 / P = 230^2 / 60 ~ 882 Ohm

