# Oscilloskopet

BNC kontakter = banankontakter!

Förberedelseuppgifter

## Syfte:

Lära sig mätmetoder för elektriska storheter som varierar med tiden. Bekanta sig med oscilloskopet och lära känna ytterligare två elektriska komponenter, kondensator och diod.

## Introducera material

* Vad är/gör ett oscilloskop? Genomgång av oscilloskopet lite snabbt.
    * Koppla in tillsammans och titta på AC spänning.
    * Kanaler och trigger, resten ska ni titta på i uppgift 1.
* Vad är diod och kondensator för elektriska komponenter?

**Utrustning**:

* Oscilloskop
* Dekadresistorer - varierbar! Kör vanliga resistorer istället.
* (Dekadkapacitanser) Kapacitans på 1 eller 10 µF istället.
* (Funktionsgenerator) Skippar, använder gul spänningskälla istället.
* Multimeter (OBS har en växelströms inställning).
* Växelspänningskälla.
* Likspänningskälla.
* Glödlampa.
* Lågohmigt motstånd.
* Dioder.

## Uppgifter

1. Bekanta sig med oscilloskopet
	* Här följer vi inte handledningen, direkt (kan löna sig att läsa igenom den ändå). M.a.o. skriv följande på tavlan.
	* a) Spänningskälla = Gul låda AC. Sätt spänningsreglaget på 6 V. 
	* b) Likt andra förberedelseuppg., bestäm frekvens, amplitud och effektivvärde för växelspänningskällan mha oscilloskopet. Använd "MEASURE"!.
	* Undersök olika alternativ: V/div, time/div, CH menu, cursor och trig menu
        
2. Mätning av spänning och ström
	* Koppla som i förberedelsuppg. 1 (använder 1 Ohms), och 6 V AC:
    * Ström amperemeter (växelmätning) = 134 mA
	* U(RMS) över motstånd = 161 mV -> 161 mA
	* De stämmer överens rätt så bra, men .... Vad beror detta på?
	* Period över motstånd = 20 ms, dvs 50 Hz.
	* Spänning över lampa med osc. = 6.8 V RMS -> Använd förberedelseuppg. för beräkning av medeleffekt. 
    
    $P = U * I = 6.8 * 0.161 \sim 1$W
    
        
3. Uppladdning av en kondensator
	* Följ instruktionerna väldigt noggrannt!! (OBS nu DC!)
	* Tidskonstant = t(0-63%) (eller t(0-~100%)/5, undvik pga osäkerhet när det sker) (exponentiell uppladdning)
	* C = 10 µF -> 5T(100 kOhm) = 4.36 s, 5T(200 kOhm) = 9.84 s
	* C = 1 µF -> 5T(100 kOhm) = 476 ms, 5T(200 kOhm) = 5 * 160 ms
	* Vilken är relationen mellan värden? Stämmer det överens med t =  RC? Kolla endast relationen, R = 100 kOhm vs 200 kOhm.
	* Oscilloskopet har en inre resistans på 1 MOhm -> denna styr vilken ström i kretsen vi får och vilken spänning vi kan ladda upp kondensatorn med. Däremot styr den inte tidskonstanten (eller?) eftersom strömmen som laddar upp kondensatorn inte går genom oscilloskopet. 
	
4. För de som hinner:
	* Skillnad mellan halvvågslikriktning och helvågslikriktning - Diod!
	* Tips: rita hur dioden (från resistorsamling) är orienterad : "pilen" är riktad mot den gråa randen.
		a) Halvvågslikriktning cuttar halva vågen
		b) Ljus starkare (no cut)
		c) Koppla sladdar till varje nod, en i taget. Utmaningen :)


Capacitors and RC circuits:
<http://www.electronics-tutorials.ws/rc/rc_1.html>

Knappar/alternativ oscilloskopet:
BW-limit: filtering out unwanted noise fq in signal
Coupling ground: A switch which DC-couples the scope input to ground could reveal whether there is an offset in the ADC which shows up as a displacement from the marker where ground should be.

The switch also disconnects the probe's signal (it will not simply short out the probe!), so you can use it as an off switch break the probe from the circuit without disconnecting it. It's possible that the probe's ground clip is also cut when the input is in that state, which is even better since it is complete isolation.
Coupling AC: Coupling through capacitor -> removes DC!
