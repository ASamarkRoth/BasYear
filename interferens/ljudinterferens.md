# Ljudinterferens

## Förberedelseuppgifter

* Vad är ultraljud? 

    | Ljud med frekvens över 20 kHz, kan inte uppfattas av människoörat.
* Vilken våglängd, i luft, har ljudvågor med frekvensen 40 kHz? Ljudets utbredningshastighet är 343 m/s. 

    | $v = \lambda f$ -> $\lambda = v/f = 343/40e3 = 8.6$ mm

    
* Två högtalare sänder ut samma ton i fas med varandra. I en godtycklig punkt P, framför högtalarna, hörs ett maximum. Avståndet från högtalare 1 till punkten P är l 1 och från högtalare 2 till punkten P är l 2 . Vad gäller för gångvägsskillnaden (l 1 - l 2 )?

    | Om $l_1-l_2 = m\lambda$ där $m = 1, 2, 3, ...$ så erhålls konstruktiv interferens och när $l_1-l_2 = 0.5m\lambda$ erhålls destruktiv interferens.

* 4. De två högtalarna i figuren nedan sänder ut samma ton i fas med varandra. En lyssnare
befinner sig i punkten P. Kommer lyssnaren att höra ett maximum (konstruktiv interferens) eller ett minimum (destruktiv interferens) då frekvensen är:
a. 1466 Hz
b. 977 Hz

   | $l1-l2 = \sqrt{1.813^2+2.200^2} + \sqrt{1.187^2+2.200^2} = 0.351$. 
   
   | $m\lambda = 343 / f * m = 0.234$ and $ = 0.351$
   
   | $350.99/ m\lambda = 1.5$ and = 1, dvs a) destruktiv och b) konstruktiv.


## Syfte
Utveckla en förståelse för konstruktiv och destruktiv interferens med ljudvågor.

## Material
Materiel: 2 ultraljudsändare, 1 ultraljudmottagare, signalgenerator, oscilloskop, linjal stativ
och koaxialkablar med T-koppling.

## Utförande

1) Injustering av frekvensen. Starta från 40 kHz och ändra i 100-tals Hz. 
    | -> 41 kHz
    
2) Interferens mellan två ljudkällor. 

a) Ljudstyrkan varierar när vi går från maxima till minima.

b) Håller för ena sändaren vid maximum -> amplitud halveras. Endast ljud från en högtalare.

c) Håller för ena sändaren vid minimum -> amplitud förstärks. Endast ljud från en högtalare.

d) l1 och l2 i 4:e maximum: 76 och 79.5 cm -> 3.5 cm. Detta medför $\lambda = 3.5/4 = 0.88$ cm. I sin tur detta ger en uppskattning på ljudets hastighet till: $v = \lambda * f = 0.85e-2 * 41e3 = 350$ m/s.
    Varför så stor skillnad?

e) 87 -> 83 cm motsvarar 10 maxima. 
Svårt eftersom pulserna vibrerar. Detta skulle motsvara längre våglängd iaf. Varför? 
I detta försök varieras endast positionen för en högtalare. I större grad får det effekten att längre avståndskorrigering behövs för att nästa maximum ska nås. 
Förmodligen avses erhålla vä

**Är gångvägsskillnaden linjär mot positionen för högtalaren?**
Do the math. 

## Note

* Olika grupper observerar brus medan andra inte gör det. Vad är typiska bruskällor för ultraljud? 
* 
